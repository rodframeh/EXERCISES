//SYSTEM
#include <stdlib.h>
//PROJECT
#include "../lib/sds.h"
#include "np_datasource.h"
#include "np_mongo.h"
//---------------------------------------------------------------------------------------------------------------------
//FUNCTIONS
np_datasource_t *np_db_datasource_init(void) {
    np_datasource_t *np_datasource;
    np_datasource = malloc(sizeof(np_datasource_t));
    if (!np_datasource)
        return NULL;
    return np_datasource;
}

void np_db_configure_datasource(np_datasource_t *np_datasource){
    if(np_datasource->np_type_database==MONGODB)
        np_db_configure_datasource_mongo(np_datasource);
}

void np_db_datasource_free(np_datasource_t *np_datasource) {

    if(np_datasource->np_type_database==MONGODB)
        np_db_datasource_free_mongo(np_datasource);

    sdsfree(np_datasource->database_host);
    sdsfree(np_datasource->database_port);
    sdsfree(np_datasource->database_name);
    sdsfree(np_datasource->database_user_name);
    sdsfree(np_datasource->database_user_password);

    if (!np_datasource)
        return;

    free(np_datasource);
}
//---------------------------------------------------------------------------------------------------------------------

