#ifndef NP_DATABASE_H
#define NP_DATABASE_H
//---------------------------------------------------------------------------------------------------------------------
//ENUM
typedef enum _np_db_type_dataset_t np_type_dataset_t;
//---------------------------------------------------------------------------------------------------------------------
//ENUM
enum _np_db_type_dataset_t {
    DOCUMENT,
    TABLE,
    HASH_STORE,
    HASH_LIST
};
//FUNCTIONS
np_datasource_t *np_db_datasource_init(void);
void np_db_configure_datasource(np_datasource_t *np_datasource);
void np_db_datasource_free(np_datasource_t *np_datasource);
//---------------------------------------------------------------------------------------------------------------------
#endif
