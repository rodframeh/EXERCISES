#ifndef NP_DATASET_H
#define NP_DATASET_H
//---------------------------------------------------------------------------------------------------------------------
//STRUCTS
typedef struct _np_db_dataset_field_t np_db_dataset_field_t;
//---------------------------------------------------------------------------------------------------------------------
//STRUCTS
struct _np_db_dataset_field_t{
    dict *row;
    char *value;
    char *column_name;
    char *column_data_type;
    char *column_name_father;
};
//---------------------------------------------------------------------------------------------------------------------
//FUNCTIONS
np_db_dataset_field_t *np_db_dataset_field_init(void);
void np_db_dataset_add_field(np_db_dataset_field_t *dict_field);
//---------------------------------------------------------------------------------------------------------------------
#endif
