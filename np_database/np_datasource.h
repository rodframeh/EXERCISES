#ifndef NP_DATASOURCE_H
#define NP_DATASOURCE_H
//---------------------------------------------------------------------------------------------------------------------
//ENUMS
typedef enum _np_db_type_database_t np_type_database_t;
//STRUCTS
typedef struct _np_db_datasource_t np_datasource_t;
//---------------------------------------------------------------------------------------------------------------------
//ENUMS
enum _np_db_type_database_t {
    MONGODB,
    REDIS
};
//STRUCTS
struct _np_db_datasource_t {
    char *database_host;
    char *database_port;
    char *database_user_name;
    char *database_user_password;
    np_type_database_t np_type_database;
    char *database_name;
    void *database_connection;
    void *database;
};
//---------------------------------------------------------------------------------------------------------------------
#endif