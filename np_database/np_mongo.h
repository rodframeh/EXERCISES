#ifndef NP_MONGO_H
#define NP_MONGO_H
//PROJECT
#include "../lib/dict.h"
//---------------------------------------------------------------------------------------------------------------------
//FUNCTIONS
void np_db_configure_datasource_mongo(np_datasource_t *np_datasource);
void np_db_datasource_free_mongo(np_datasource_t *np_datasource);
void np_db_repository_insert_mongodb(np_datasource_t *np_datasource,char *dataset_name, dict *data);
//---------------------------------------------------------------------------------------------------------------------
#endif
