//SYSTEM
#include <stdlib.h>
#include <string.h>
//PROJECT
#include "../lib/dict.h"
#include "../lib/sds.h"
#include "np_dictionary.h"
//---------------------------------------------------------------------------------------------------------------------
//FUNCTIONS
uint64_t np_ds_dict_sds_hash(const void *key) {
    return dictGenHashFunction((unsigned char *) key, sdslen((char *) key));
}

int np_ds_dict_sds_key_compare(void *privdata, const void *key1, const void *key2) {
    int l1, l2;
    DICT_NOTUSED(privdata);

    l1 = sdslen((sds) key1);
    l2 = sdslen((sds) key2);

    if (l1 != l2) return 0;
    return memcmp(key1, key2, l1) == 0;
}

void np_ds_dict_sds_destructor(void *privdata, void *val) {
    DICT_NOTUSED(privdata);
    sdsfree(val);
}

unsigned int np_ds_dict_key_hash(const void *keyp) {
    unsigned long key = (unsigned long)keyp;
    key = dictGenHashFunction(&key,sizeof(key));
    key += ~(key << 15);
    key ^=  (key >> 10);
    key +=  (key << 3);
    key ^=  (key >> 6);
    key += ~(key << 11);
    key ^=  (key >> 16);
    return key;
}

int np_ds_dict_key_compare(void *privdata, const void *key1, const void *key2) {
    unsigned long k1 = (unsigned long)key1;
    unsigned long k2 = (unsigned long)key2;
    return k1 == k2;
}

dictType *np_ds_dict_type_init(void){
    dictType *dict_type;
    dict_type = malloc(sizeof(dictType));
    if (!dict_type)
        return NULL;
    return dict_type;
}

dict *np_ds_dict_init(void) {
    /* hash function */
    /* key dup */
    /* val dup */
    /* key compare */
    /* key destructor */
    /* val destructor */
    dictType *dict_type;
    dict_type= np_ds_dict_type_init();
    dict_type->hashFunction=np_ds_dict_sds_hash;
    dict_type->keyCompare=np_ds_dict_sds_key_compare;
    dict_type->keyDestructor=np_ds_dict_sds_destructor;
    dict_type->valDestructor=np_ds_dict_sds_destructor;
    dict_type->keyDup=NULL;
    dict_type->valDup=NULL;

    return dictCreate(dict_type, NULL);
}

void np_ds_dict_free(dict *d) {
    dictRelease(d);
}
//---------------------------------------------------------------------------------------------------------------------
