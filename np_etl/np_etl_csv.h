#ifndef NP_ETL_CSV_H
#define NP_ETL_CSV_H

typedef struct _np_etl_csv_t np_etl_csv_t;
typedef struct _np_etl_csv_column_t np_etl_csv_column_t;

struct _np_etl_csv_column_t {
    int position_column_from;
    char *name_column_to;
};

struct _np_etl_csv_t {
    char *path;
    np_etl_csv_column_t *np_etl_csv_column;
};

#endif
